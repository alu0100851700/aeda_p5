#include "est_ford.hpp"
#include "dni.hpp"
#include <iostream>
#include <cstdlib>
using namespace std;


int main(void){

	int n=0;
	do{
		cout << "Tamaño de la secuencia: ";
		cin >> n;
	}while(n<1);
	
	int nPruebas=0;
	do{
		cout << "Numero de pruebas: ";
		cin >> nPruebas;
	}while(nPruebas<1)

	
	//Creación del array que contiene las funciones de ordenación
	ford<dni>** f = new ford<dni>*[5];
	f[0] = new fInsercion<dni>;
	f[1] = new fSeleccion<dni>;
	f[2] = new fShellSort<dni>;
	f[3] = new fQuickSort<dni>;
	f[4] = new fMergeSort<dni>;

	//Creación del array que contiene los dni
	dni* a_dni;

	//Ejecución de pruebas
	cout << "\t\tMinimo" << "\tMedio" << "\tMaximo" << endl;
	//for que varia entre las funciones de ordenacion
	for(int i=0; i<5; i++){
		int min=0, med=0, max=0;

		//for que varia entre las pruebas a ejecutar
		for(int j=0; j<nPruebas; j++){
			//Creando dnis para esta prueba
			a_dni = new dni[n];

			int nComp = (f[i] -> run(a_dni,n));

			if(nComp < min || min == 0)	min = nComp;
			if(nComp > max || max == 0) max = nComp;
			med += nComp;
			nComp = 0;

			//Borrando dnis de esta prueba
			delete[] a_dni;
		}
		med /= nPruebas;


		switch (i){
		case 0:
			cout << "Insercion";
			break;
		case 1:
			cout << "Seleccion";
			break;
		case 2:
			cout << "ShellSort";
			break;
		case 3:
			cout << "QuickSort";
			break;
		case 4:
			cout << "MergeSort";
			break;
		default:
			break;
		}

		cout << "\t" << min << "\t" << med << "\t" << max << "\n" << endl;

	}

	delete[] f;


	return 0;
}