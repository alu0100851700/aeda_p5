#pragma once
#include <iostream>
using namespace std;
#define DEMO

#ifdef DEMO
template <class clave>
void printv(clave* v, const int& nClaves){
	cout << "[";
	int i;
	for(i=0; i<nClaves-1; i++)
		cout << v[i] << ", ";
	cout << v[i] << "]" << endl;
}

template <class clave>
void printv(clave* v, const int& ini, const int& fin){
	cout << "[";
	int i;
	for(i=ini; i<fin; i++)
		cout << v[i] << ", ";
	cout << v[i] << "]" << endl;
}
#endif

template <class clave>
class ford{
public:
	ford(){}
	~ford(){}
	virtual void run(clave*, const int&) = 0;
};

template <class clave>
class fInsercion: public ford<clave>{
public:
	void run(clave* v, const int& nClaves){
		#ifdef DEMO
			printv(v,nClaves);
			cin.get();
		#endif

		for(int i=1; i<nClaves; i++){
			int j=i;
			clave x = v[i];

			#ifdef DEMO
				cin.get();
				cout << "x = " << "v[" << (i) << "]" << endl;
			#endif

			while(x < v[j-1] && j>0){

				#ifdef DEMO
					cin.get();
					cout << "(x)" << x << " < " <<  "v[" << (j-1) << "]" << " = " << (x < v[j-1]? "true":"false") << endl;
					cout  << "v[" << j << "]" << " = " << "v[" << (j-1) << "]" << endl;
				#endif

				v[j] = v[j-1];

				#ifdef DEMO
					printv(v,nClaves);
				#endif

				j--;
			}
			v[j] = x; 

			#ifdef DEMO
				cin.get();
				cout  << "v[" << j << "]" << " = (x)" << x << endl;
				printv(v,nClaves);
			#endif
		}
	}
};

template <class clave>
class fSeleccion: public ford<clave>{
public:
	void run(clave* v, const int& nClaves){
		#ifdef DEMO
			printv(v,nClaves);
			cin.get();
		#endif

		for(int i=0; i<nClaves; i++){
			int min=i;

			for(int j=i; j<nClaves; j++){

				#ifdef DEMO
					cout  << "v[" << j << "]" << " < " << "v[" << min << "]" << " = "<< (v[j]< v[min]? "true":"false") << endl;
				#endif
				if(v[j] < v[min]) {
					#ifdef DEMO
						cout  << "v[" << min << "]" << " = " << "v[" << j << "]" << endl;
					#endif
					min = j;

					#ifdef DEMO
						printv(v,nClaves);
						cin.get();
					#endif
				}

				#ifdef DEMO
					cout << "v[" << min << "]" << " <---> " <<  "v[" << i << "]" << endl;
					printv(v,nClaves);
					cin.get();
				#endif
				clave x = v[min];
				v[min] = v[i];
				v[i] = x;
			}
		}
	}
};

template <class clave>
class fShellSort: public ford<clave>{
public:
	void run(clave* v, const int& nClaves){
		
		float d=0;
		while(d <= 0 || d >= 1){
			cout << "delta: ";
			cin >> d;
		}
		int delta = (int)(d*nClaves);

		#ifdef DEMO
			printv(v,nClaves);
			cin.get();
		#endif

		while(delta > 1){
			#ifdef DEMO
				cout << "delta= " << d << endl;
			#endif 
			deltasort(v,nClaves,delta);
			delta /= 2;
		}
	}

	void deltasort(clave* v, const int& nClaves, const int& d){
		for(int i=d; i<nClaves; i++){
			#ifdef DEMO
				cin.get();
				cout << "x = v[" << (i) << "]" << endl;
			#endif
			clave x = v[i];

			int j = i;

			#ifdef DEMO
				cout << "(x)" << x << " < v[" << (int)(j-d) << "] = " << (x<v[j-d]? "true":"false")<< endl;
				cin.get();
			#endif

			while(j>d && x<v[j-d]){

				v[j] = v[j-d];
				j = j-d;

				#ifdef DEMO
					cout << "v[" << j << "] = v[" << j-d << "]" << endl;
					printv(v,nClaves);
					cin.get();
				#endif
			}
			v[j] = x;
			#ifdef DEMO
				cout << "v[" << j << "] = (x)" << x << endl;
				printv(v,nClaves);
			#endif
		}
	}
};

template <class clave>
class fQuickSort: public ford<clave>{
public:
	void run(clave* v, const int& nClaves){
		Qsort(v, 0, nClaves-1);
	}

private:
	void Qsort(clave* v, const int& ini, const int& fin){
		#ifdef DEMO
			cout << "--- SUBVECTOR v[" << ini << "] to v[" << fin << "] ---" << endl;
			printv(v,ini,fin);
			cin.get();
		#endif

		int i=ini, f=fin;
		clave p = v[(i+f)/2];

		#ifdef DEMO
			cout << "p = v[" << ((i+f)/2) << "] = " << v[(i+f)/2] << endl; 
			cin.get();
		#endif

		while(i <= f){
			while(v[i] < p) i++;
			while(v[f] > p) f--;

			if(i<=f){
				clave x=v[i];
				v[i] = v[f];
				v[f] = x;

				#ifdef DEMO
					cout << "v[" << i << "] <---> " << "v[" << f << "]" << endl;
					printv(v,ini,fin);
					cin.get();
				#endif

				i++; f--;
			}
		}
		if(ini < f) Qsort(v, ini, f);
		if(i < fin) Qsort(v, i, fin);
	}
};

template <class clave>
class fMergeSort: public ford<clave>{
public:
	void run(clave* v, const int& nClaves){
		#ifdef DEMO
			printv(v,nClaves);
			cin.get();
			cout << endl;
		#endif

		Msort(v, 0, nClaves-1);

		#ifdef DEMO
			printv(v,nClaves);
			cin.get();
			cout << endl;
		#endif
	}

private:
	void Msort(clave* v, const int& ini, const int& fin){
		if(ini < fin){
			int cen = (ini + fin)/2;
			Msort(v, ini, cen);
			Msort(v, cen+1, fin);
			#ifdef DEMO
				cout << "\n--- SUBVECTOR v[" << ini << "] to v[" << fin << "] ---" << endl;
				printv(v,ini,fin);
				cin.get();
			#endif
			Mezcla(v, ini, cen, fin);
		}
	}

	void Mezcla(clave* v, const int& ini, const int& cen, const int& fin){
		clave aux[fin];
		int i=ini, j=cen+1, k=ini;
		while( i <= cen && j <= fin){
			
			#ifdef DEMO
			cout << "v[" << i << "] < v[" << j << "] = " << (v[i] < v[j]? "true":"false") << endl;
			cout << "aux[" << k << "] = v[" << (v[i] < v[j]? i:j) << "]" << endl;
			cin.get();
			#endif
			
			if(v[i] < v[j]){
				aux[k] = v[i];
				i++;
			}
			else{
				aux[k] = v[j];
				j++;
			}
			k++;

			#ifdef DEMO
				cout << "aux: ";
				printv(aux,ini,k-1);
				cin.get();
			#endif
		}

		if(i > cen)
			while(j <= fin){
				#ifdef DEMO
					cout << "aux[" << k << "] = v[" << j << "]" << endl;
				#endif
				aux[k] = v[j];
				j++;	k++;
			}
		else
			while(i <= cen){
				#ifdef DEMO
					cout << "aux[" << k << "] = v[" << i << "]" << endl;
				#endif
				aux[k] = v[i];
				i++; k++;
			}

		#ifdef DEMO
			cout << "aux: ";
			printv(aux,ini,k-1);
			cin.get();
		#endif

		for(k=ini; k<=fin; k++)
			v[k] = aux[k];
	}
};

