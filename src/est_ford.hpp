#pragma once
#include <iostream>
using namespace std;

template <class clave>
class ford{
public:
	ford(){}
	~ford(){}
	virtual int run(clave*, const int&) = 0;
};

template <class clave>
class fInsercion: public ford<clave>{
public:
	int run(clave* v, const int& nClaves){
		int nComp=0;

		for(int i=1; i<nClaves; i++){
			int j=i;
			clave x = v[i];

			nComp++;
			while(x < v[j-1] && j>0){
				v[j] = v[j-1];
				j--;

				nComp++;
			}
			v[j] = x; 
		}
		return nComp;
	}
};

template <class clave>
class fSeleccion: public ford<clave>{
public:
	int run(clave* v, const int& nClaves){
		int nComp=0;

		for(int i=0; i<nClaves; i++){
			int min=i;

			for(int j=i; j<nClaves; j++){

				nComp++;
				if(v[j] < v[min]) min = j;

			clave x = v[min];
			v[min] = v[i];
			v[i] = x;
			}

		}
		return nComp;
	}
};

template <class clave>
class fShellSort: public ford<clave>{
public:
	int run(clave* v, const int& nClaves){
		int nComp=0;
		float d=0.7;
		int delta = (int)(d*nClaves);
		while(delta > 1){
			nComp += deltasort(v,nClaves,delta);
			delta /= 2;
		}
		return nComp;
	}

	int deltasort(clave* v, const int& nClaves, const int& d){
		int nComp = 0;
		for(int i=d+1; i<nClaves; i++){
			clave x = v[i];
			int j = i;

			nComp++;
			while(j>d && x<v[j-d]){
				v[j] = v[j-d];
				j -= d;

				nComp++;
			}
			v[j] = x;
		}
		return nComp;
	}
};

template <class clave>
class fQuickSort: public ford<clave>{
public:
	int run(clave* v, const int& nClaves){
		int nComp=0;
		nComp += Qsort(v, 0, nClaves-1);
		return nComp;
	}

private:
	int Qsort(clave* v, const int& ini, const int& fin){
		int nComp=0;
		int i=ini, f=fin;
		clave p = v[(i+f)/2];
		while(i < f){
			nComp++;
			while(v[i] < p){
				i++;
				nComp++;
			} 

			nComp++;
			while(v[f] > p){
				f--;
				nComp++;
			}

			if(i<=f){
				clave x=v[i];
				v[i] = v[f];
				v[f] = x;
				i++; f--;
			}
		}
		if(ini < f) Qsort(v, ini, f);
		if(i < fin) Qsort(v, i, fin);

		return nComp;
	}
};

template <class clave>
class fMergeSort: public ford<clave>{
public:
	int run(clave* v, const int& nClaves){
		int nComp=0;

		nComp += Msort(v, 0, nClaves-1);

		return nComp;
	}

private:
	int Msort(clave* v, const int& ini, const int& fin){
		int nComp=0;
		if(ini < fin){
			int cen = (ini + fin)/2;
			nComp += Msort(v, ini, cen);
			nComp += Msort(v, cen+1, fin);
			nComp += Mezcla(v, ini, cen, fin);
		}
		return nComp;
	}

	int Mezcla(clave* v, const int& ini, const int& cen, const int& fin){
		int nComp = 0;
		clave aux[fin];
		int i=ini, j=cen+1, k=ini;
		while( i <= cen && j <= fin){
			nComp++;
			if(v[i] < v[j]){
				aux[k] = v[i];
				i++;
			}
			else{
				aux[k] = v[j];
				j++;
			}
			k++;
		}

		if(i > cen)
			while(j <= fin){
				aux[k] = v[j];
				j++;	k++;
			}
		else
			while(i <= cen){
				aux[k] = v[i];
				i++; k++;
			}

		for(k=ini; k<=fin; k++)
			v[k] = aux[k];

		return nComp;
	}
};