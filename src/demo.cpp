#include "demo_ford.hpp"
#include "dni.hpp"
#include <iostream>
#include <cstdlib>
using namespace std;

int main(void){

	int n=0;
	do{
		cout << "Tamaño de la secuencia: ";
		cin >> n;
	}while(n<1 || n>25);

	//Creando array con dni
	dni* a_dni = new dni[n];
	
	ford<dni>* f;
	int case_f;
	cout << "\nFuncion de ordenacion:"	<< endl
		<< "1. Interseccion"			<< endl
		<< "2. Seleccion"				<< endl
		<< "3. ShellSort" 				<< endl
		<< "4. QuickSort"	 			<< endl
		<< "5. HeapSort"	 			<< endl;

	//Inicializando funcion elegida
	cin >> case_f;
	switch (case_f){
		case 1:
			f = new fInsercion<dni>;
			break;
		case 2:
			f = new fSeleccion<dni>;
			break;
		case 3:
			f = new fShellSort<dni>;
			break;
		case 4:
			f = new fQuickSort<dni>;
			break;
		case 5:
			f = new fMergeSort<dni>;
			break;
		default:
			cerr << "Elección incorrecta" << endl;
			break;
	}

	//Ejecutando funcion elegida
	f -> run(a_dni,n);

	delete f;
	delete[] a_dni;

	return 0;
}